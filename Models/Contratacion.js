'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ContratacionSchema = Schema({
    Titulo: String,
    Fecha: Date,
    Descripcion: String,
    Archivo: {
        Tipo: {
            type: String
        },
        Link:{
            type: String,   
        }
    },
    Paga: Shema.Types.Decimal128,
    Estado: String,
    Locacion: String,
    Participantes:{
        type: Schema.ObjectId, ref: 'Publicacion'
    },
    Reportes: Number
})

module.exports = mongoose.model('Publicacion', PublicacionSchema)