'use strict'
const mongoose = require('mongoose')
const Shema = mongoose.Shema

const PublicacionSchema = Schema({
    Titullo: String,
    Fecha: Date,
    Descripcion: String,
    Precio: Shema.Types.Decimal128,
    
    Archivo:{
        tipo:{
            type: String
        },
        link:{
            type: String
        }
    },

    Artista:{
        type: Schema.ObjectId, ref:'Usuario'
    },

    Comentario: {
        comentario:{
            type: String
        },

        usuario: {
            type: Schema.ObjectId, ref: 'Usuario'
        },

        fecha:{
            type: String
        },

        likes: {
            like:{
                type: Number,
            },
            usuario: {
                type: Schema.ObjectId, ref: 'Usuario'
            }
        },
    },

    Likes: {
        like: Number, 
        usuario: {
            type: Schema.ObjectId, ref: 'Usuario'
        }      
    },

    Reportes: Number,

})

module.exports = mongoose.model('Usuario', UsuarioSchema)