'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UsuarioSchema = Schema({

    UserName: {
        type: String,
        require: true
    }, 

    Password: {
        type: String,
        require: true
    },

    Nombre: {
        type: String,
        require: true
    },

    Apellido: {
        type: String,
        require: true
    },

    NombreArtistico: String,
    TipoArtista: {
        type: String, 
        enum: ['Baile', 'Musica','Pintura', 'Escultura', 'Actuacion', 'Literatura', 'Cinematografia', 'Fotografia', 'Historieta']
    },

    Ciudad: {
        type: String,
        require: true
    },

    Rol: {
        type: String,
        enum: ['usuario','admin','artista']
    },

    Correo: {
        correo: {
            type: String,
            require: true
        },
        descripcion: {
            type:String,
            require: true
        }
    },

    Telefono: {
        telefono: {
            type: String,
            require: true
        },
        descripcion: {
            type:String,
            require: true
        }
    },

    RedesSociales: {
        redSocial: {
            type: String
        },
        link: {
            type: String
        }
    },

    Foto: String,

    Puntos: Number,

    Seguidores: {
        type: Schema.ObjectId, ref:'Usuario'
    },

    Criticas: {
        comentario: {
            type: String
        },
        usuario: {
            type: Schema.ObjectId, ref: 'Usuario'
        },
        Fecha: {
            type: Date
        }
    },
    
    Reportes: Number
})

module.exports = mongoose.model('Usuario', UsuarioSchema)