'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()
const port = process.env.PORT || 3000
const DB = 'mongodb://pariet:admin@ds243049.mlab.com:43049/pariet'

const Usuario = require('./Models/Usuario')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/api/usuario', (req, res) => {
    console.log('Obtener Todo')
})

app.get('/api/usuario/:usuarioId', (req, res) => {
    
})

app.post('/api/usuario', (req, res) => {
    console.log('POST /api/usuario')
    console.log(req.body)
    let usuario = new Usuario()
    usuario.UserName = req.body.UserName

})

app.put('/api/usuario/:usuarioId', (req, res) => {
    
})

app.delete('/api/usuario/:usuarioId', (req, res) => {
    
})

mongoose.connect(DB, (err, res) => {
    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    console.log("Conexion a la base de Datos Establecida")
})

app.listen(port, () => {
    console.log(`API REST corriendo en http://localhost:${port}`)
})